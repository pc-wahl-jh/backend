from json import dumps, loads

from flask import Blueprint, Response, request

from FileManager import FileManager

parteien = Blueprint("parteien", __name__)


@parteien.route("/", methods=["GET"])
def get_parteien():
    state = FileManager.instance.get_state()
    ps = list(filter(lambda key: state[key]["type"] == "partei", state))

    parteien_dict = {}
    for key in ps:
        parteien_dict[key] = state[key]
    print(parteien_dict)

    return Response(dumps(parteien_dict, ensure_ascii=False), mimetype="application/json")


@parteien.route("/<path:partei>", methods=["GET"])
def get_partei(partei):
    state = FileManager.instance.get_state()

    ps = filter(lambda key: state[key]["type"] == "partei", state)
    print(ps)
    p = next(filter(lambda key: key == partei, ps))

    return Response(dumps(state[p], ensure_ascii=False), mimetype="application/json")


@parteien.route("/<partei>", methods=["PUT"])
def update_partei(partei):
    json = FileManager.instance.get_json()

    print(request.get_json())
    json.append({
        "UUID": partei,
        "changes": loads(request.data)
    })
    FileManager.instance.set_json(json)

    return Response()
