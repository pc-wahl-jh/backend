import json

import collections


class FileManager:
    instance = None

    def __init__(self, file_name):
        self.filename = file_name

    def get_json(self):
        with open(self.filename, encoding="utf-8") as file:
            file_content = file.read()
        return json.loads(file_content)

    def set_json(self, new_json):
        with open(self.filename, "w", encoding="utf-8") as file:
            file.write(json.dumps(new_json, ensure_ascii=False, indent=4))

    def get_state(self):
        state = {}
        print(self.get_json())
        for update in self.get_json():
            if update["UUID"] not in state:
                state[update["UUID"]] = {}
            state[update["UUID"]] = self.update(state[update["UUID"]], update["changes"])
        return state

    def update(self, d, u):
        for k, v in u.items():
            if isinstance(v, collections.Mapping):
                r = self.update(d.get(k, {}), v)
                d[k] = r
            else:
                d[k] = u[k]
        return d
